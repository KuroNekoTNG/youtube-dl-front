﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace YouTubeDL.Front.Utilities {
	internal class YtdlOutput {

		public readonly struct Format {

			public uint FormatCode {
				get;
			}

			public string Extension {
				get;
			}

			public string Resolution {
				get;
			}

			public string Notes {
				get;
			}

			public Format( string formatCode, string ext, string res, string notes ) : this( 0, ext, res, notes ) {
				FormatCode = uint.Parse( formatCode );
			}

			public Format( uint formatCode, string ext, string res, string notes ) {
				FormatCode = formatCode;
				Extension = ext;
				Resolution = res;
				Notes = notes;
			}
		}

		private const string FULL_FORMAT_SECTION_GRAB = @"([info\] .+ \(best\))";
		private const string ID_REGEX_GRAB = @"\[info\] Available formats for (\S{11}):\n.+\n(?:[0-9]{2,3}.+\n?)+ \(best\)";
		private const string FORMAT_DATA_GRAB = @"(?:(\d+)\s+(\w+)\s+(\w+\s?\w+)\s+(.+))(?: \(best\))?";

		public static YtdlOutput GetOutputData( string data ) {
			var idMatch = Regex.Match( data, ID_REGEX_GRAB );
			var lineMatches = Regex.Matches( idMatch.Value, FORMAT_DATA_GRAB );
			var availableFormts = new List<Format>();

			for ( int m = 0; m < lineMatches.Count; ++m ) {
				var match = lineMatches[m];

				for ( int g = 1; g < match.Groups.Count; ++g ) {
					var group = match.Groups[g];
					var format = new Format( group.Captures[1].Value, group.Captures[2].Value, group.Captures[3].Value, group.Captures[4].Value );

					availableFormts.Add( format );
				}
			}

			return new YtdlOutput( idMatch.Groups[1].Value, availableFormts );
		}

		public static IEnumerable<YtdlOutput> GetOutputDatas( string data ) {
			var matches = Regex.Matches( data, FULL_FORMAT_SECTION_GRAB );
			var outputList = new LinkedList<YtdlOutput>();

			foreach ( Match match in matches ) {
				var output = GetOutputData( match.Value );

				outputList.AddLast( output );
			}

			return outputList;
		}

		public string VideoID {
			get;
		}

		public IReadOnlyList<Format> AvailableFormts => availableFormts.AsReadOnly();

		private List<Format> availableFormts;

		private YtdlOutput( string videoID ) {
			VideoID = videoID;
			availableFormts = new List<Format>();
		}

		private YtdlOutput( string videoID, IEnumerable<Format> formats ) : this( videoID ) {
			availableFormts.AddRange( formats );
		}
	}
}
