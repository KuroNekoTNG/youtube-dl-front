﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using YouTubeDL.Front.UI;

namespace YouTubeDL.Front.Utilities {
	public static class YouTubeDLHandler {

		public const string YOUTUBE_DL_LATEST = "https://youtube-dl.org/latest";
		public const string YOUTUBE_DL_LATEST_EXECUTABLE = "https://youtube-dl.org/downloads/latest/youtube-dl.exe";

		public static string YouTubeDLFolder {
			get;
		} = Path.Combine(
			Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ),
			"KuroNeko Kyubi Felis",
			"YouTube-DL Front",
			"youtube-dl"
		);

		public static string YouTubeDLFile {
			get;
		} = Path.Combine( YouTubeDLFolder, "youtube-dl.exe" );

		public static bool YouTubeDLFileExists => File.Exists( YouTubeDLFile );

		public static bool YouTubeDLFolderExists => Directory.Exists( YouTubeDLFolder );

		public static bool YouTubeDLUpdate {
			get {
				if ( !YouTubeDLFileExists ) {
					return true;
				}

				try {
					youtubeDLProc.StartInfo.Arguments = "--version";
					youtubeDLProc.Start();

					var curVer = youtubeDLProc.StandardOutput.ReadToEnd().Trim();
					var webVer = GetWebVersion();

					return curVer != webVer;
				} catch {
					return true;
				}
			}
		}

		private static Process youtubeDLProc;

		static YouTubeDLHandler() {
			youtubeDLProc = new Process() {
				StartInfo = new ProcessStartInfo( YouTubeDLFile ) {
					CreateNoWindow = true,
					UseShellExecute = false,
					RedirectStandardOutput = true,
					RedirectStandardError = true
				}
			};
		}

		public static void ForceInstallYouTubeDL() => StartUpdate( null );

		public static void UpdateYouTubeDL( ProgressWindow progress ) {
			progress.SetProgress( "Checking for update. . .", -1, false );

			if ( !YouTubeDLUpdate ) {
				progress.SetProgress( "Done", -1f, true );

				return;
			}

			try {
				StartUpdate( progress );
			} catch ( Exception e ) {
				if ( Debugger.IsAttached && Debugger.IsLogging() ) {
					Debug.WriteLine( $"An exception occured:\nException: {e.GetType().FullName}\nMessage: {e.Message}\nStacktrace:\n{e.StackTrace}" );
				}

				progress.SetProgress( "", -1, true );

				throw;
			}

			progress.SetProgress( "Done", -1, true );
		}

		private static void StartUpdate( ProgressWindow progress ) {
			if ( !YouTubeDLFolderExists ) {
				Directory.CreateDirectory( YouTubeDLFolder );
			}

			using ( var client = new WebClient() ) {
				var task = client.DownloadFileTaskAsync( YOUTUBE_DL_LATEST_EXECUTABLE, YouTubeDLFile );
				string len = string.Empty;

				client.DownloadProgressChanged += ( _, e ) => {
					var downloaded = ( e.TotalBytesToReceive / 1024f ).ToString( "0.00" );
					var percent = e.ProgressPercentage / 100d;
					var len1 = string.Empty;

					// Attempts to get the size of the file being downloaded.
					if ( int.TryParse( client.ResponseHeaders[HttpResponseHeader.ContentLength], out var lenInt ) ) {
						len1 = ( lenInt / 1024d ).ToString( "0.00" );
					}

					progress.SetProgress( $"Downloaded {downloaded} KiB / {len} KiB.", percent, false );
				};

				// Waits until the task is done.
				task.Wait();
			}
		}

		private static string GetWebVersion() {
			var link = YOUTUBE_DL_LATEST;

			// Attempts to grab the URL to the latest version of YouTube-DL.
			do {
				var req = WebRequest.Create( link );
				req.Method = "HEAD";

				var res = req.GetResponse();

				link = res.ResponseUri.AbsoluteUri;
			} while ( !link.Contains( "github.com" ) );

			// Attempts to grab/find the version within the URL, thanks GitHub for that!
			var release = Regex.Match( link, "https?://github.com/ytdl-org/youtube-dl/releases/tag/([0-9]{4}.[0-9]{2}.[0-9]{2})/?" ).Groups[1].Value;

			return release;
		}
	}
}
