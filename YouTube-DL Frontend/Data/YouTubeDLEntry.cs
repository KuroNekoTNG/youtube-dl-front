﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeDL.Front.Data {
	public class YouTubeDLEntry {

		public string URL {
			get;
			set;
		}

		public string Output {
			get;
			set;
		}

		public bool Audio {
			get;
			set;
		}

		public bool Video {
			get;
			set;
		}

		public YouTubeDLEntry( string url, string output, bool audio = true, bool video = false ) {
			URL = url;
			Output = output;
			Audio = audio;
			Video = video;
		}

	}
}
