﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Windows;

using YouTubeDL.Front.UI;
using YouTubeDL.Front.Utilities;

using File = System.IO.File;

/* Place this in an update section.
 */

namespace YouTubeDL.Front {
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application {

		public static bool IsDebug =>
#if DEBUG
				true;
#else
				false;
#endif


		private bool IsElevated {
			get {
				bool elevated;

				using ( var id = WindowsIdentity.GetCurrent() ) {
					var principal = new WindowsPrincipal( id );

					elevated = principal.IsInRole( WindowsBuiltInRole.Administrator );
				}

				return elevated;
			}
		}

		protected override void OnStartup( StartupEventArgs e ) {
			base.OnStartup( e );

			if ( IsDebug && !Debugger.IsAttached ) {
				Debugger.Launch();
			}

			// Will only check args if it is not a debug build.
			if ( !IsDebug ) {
				CheckArgs( e );
			}

			var progWindow = new ProgressWindow( YouTubeDLHandler.UpdateYouTubeDL );

			progWindow.Start();

			MainWindow = new MainWindow();

			MainWindow.Show();
		}

		private void CheckArgs( StartupEventArgs e ) {
			if ( e.Args.Contains( "--install" ) ) {
				InstallProgram();
				return;
			}

			if ( e.Args.Contains( "--continue-install" ) ) {
				var oldLoc = e.Args.Where( s => s.StartsWith( "--old-location=" ) )
					.First()
					.Split( '=' )[1];

				ResumeInstall( oldLoc );

				return;
			}

			if ( YouTubeDLHandler.YouTubeDLFolderExists ) {
				var result = MessageBox.Show( "It appears that this program was recently downloaded, would you like to do a full install?",
					"Not fully installed",
					MessageBoxButton.YesNo,
					MessageBoxImage.Question,
					MessageBoxResult.Yes
				);

				switch ( result ) {
					case MessageBoxResult.Yes:
						Install();
						break;
					case MessageBoxResult.No:
						break;
				}
			}
		}

		private void ResumeInstall( string oldLoc ) {
			if ( File.Exists( oldLoc ) ) {
				File.Delete( oldLoc );
			}

			YouTubeDLHandler.ForceInstallYouTubeDL();
			CreateShortcut();

			Shutdown();
		}

		private void CreateShortcut() {
			var desktopLoc = Environment.GetFolderPath( Environment.SpecialFolder.DesktopDirectory );
			var appLoc = Assembly.GetExecutingAssembly().Location;
			var icon = appLoc.Replace( "\\", "/" );

			using ( var linkWriter = new StreamWriter( Path.Combine( desktopLoc, "YouTube-DL Front.url" ) ) ) {
				linkWriter.WriteLine( $"[InternetShortcut]\nURL={appLoc}\nIconIndex=0\nIconFile={icon}" );
				linkWriter.Flush();

			}
		}

		private void InstallProgram() {
			const string installLoc = @"C:\Program Files\KuroNeko Kyubi Felis\YouTube-DL Front";
			const string finalExeLoc = @"C:\Program Files\KuroNeko Kyubi Felis\YouTube-DL Front\YouTube-DL Front.exe";

			if ( !Directory.Exists( installLoc ) ) {
				Directory.CreateDirectory( installLoc );
			}

			if ( File.Exists( finalExeLoc ) ) {
				File.Delete( finalExeLoc );
			}

			File.Copy( Assembly.GetEntryAssembly().Location, finalExeLoc );

			Process.Start( finalExeLoc, $"--continue-install --old-location=\"{Assembly.GetEntryAssembly().Location}\"" );

			Shutdown();
		}

		private void Install() {
			if ( !IsElevated ) {
				using ( var proc = new Process() {
					StartInfo = new ProcessStartInfo( Assembly.GetEntryAssembly().Location, "--install" ) {
						Verb = "runas",
						WorkingDirectory = Environment.CurrentDirectory,
						UseShellExecute = true
					}
				} ) {
					proc.Start();

					Shutdown();
				}
			}
		}

		// DO NOT REMOVE METHOD!
		// This method is here so the project can compile.
		// Why does this empty method need to be here to compile?
		// Because without it, it "Cannot find MainWindow.xaml.cs" for some reason.
		private void StartupEvent( object sender, StartupEventArgs e ) {

		}
	}
}