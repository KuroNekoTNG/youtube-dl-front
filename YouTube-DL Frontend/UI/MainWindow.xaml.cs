﻿using System;
using System.Collections.ObjectModel;
using System.Windows;

using YouTubeDL.Front.Data;

namespace YouTubeDL.Front.UI {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {

		private ObservableCollection<YouTubeDLEntry> entries;

		public MainWindow() {
			InitializeComponent();

			entries = new ObservableCollection<YouTubeDLEntry>();

			queueList.ItemsSource = entries;
		}

		// This is the main window, and as such, will be used to force close the program.
		protected override void OnClosed( EventArgs e ) {
			base.OnClosed( e );

			Application.Current.Shutdown();
		}

		private void Delete_Click( object sender, RoutedEventArgs e ) {

		}

		private void Download_Click( object sender, RoutedEventArgs e ) {

		}

		private void Add_Click( object sender, RoutedEventArgs e ) {
			var link = URLGrabWindow.GrabURLFromUser();
		}
	}
}
