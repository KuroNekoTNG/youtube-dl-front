﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace YouTubeDL.Front.UI {
	/// <summary>
	/// Interaction logic for URLPuller.xaml
	/// </summary>
	public partial class URLGrabWindow : Window {

		public static string GrabURLFromUser() {
			var window = new URLGrabWindow();

			return window.ShowDialog();
		}

		public URLGrabWindow() {
			InitializeComponent();
		}

		public new string ShowDialog() {
			base.ShowDialog();

			return urlEntry.Text;
		}

		private void AddClicked( object sender, RoutedEventArgs e ) {
			Close();
		}
	}
}
