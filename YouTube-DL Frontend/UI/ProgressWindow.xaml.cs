﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace YouTubeDL.Front.UI {
	/// <summary>
	/// Interaction logic for ProgressWindow.xaml
	/// </summary>
	public partial class ProgressWindow : Window {

		/// <summary>
		/// The action to invoke once starting the window.
		/// </summary>
		public Action<ProgressWindow> CallTo {
			get;
			set;
		}

		private bool close;

		/// <summary>
		/// Brings up a progress window that will show what is going on to the user.
		/// </summary>
		/// <param name="callTo">A method that returns a <see cref="bool"/> on rather it completed or not. Also needs to take in an action to call back to.</param>
		public ProgressWindow( Action<ProgressWindow> callTo ) : this() {
			CallTo = callTo;
		}

		public ProgressWindow() {
			InitializeComponent();
		}

		public void Start() {
			var thread = new Thread( () => CallTo.Invoke( this ) ) {
				Priority = ThreadPriority.Lowest,
				Name = "Progress Window Thread"
			};

			thread.Start();

			ShowDialog();
		}

		protected override void OnClosing( CancelEventArgs e ) {
			// Checks to see if closing was done via an internal method.
			if ( close ) {
				base.OnClosing( e );
			}

			// Stops the window from closing.
			//e.Cancel = true;
		}

		public void SetProgress( string action, double progress, bool done ) {
			if ( Dispatcher.Thread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId ) {
				Dispatcher.Invoke( () => SetProgress( action, progress, done ), DispatcherPriority.Normal );
				return;
			}

			// Checks if the done bool is set to true.
			if ( done ) {
				close = true;

				Close();

				return;
			}

			// Will show please wait instead of nothing if nothing is in the action param.
			textStatus.Text = string.IsNullOrWhiteSpace( action ) ? "Please Wait. . ." : action;

			// Checks to see if the progress bar should show indeterminate or not.
			if ( progress < 0 ) {
				this.progress.IsIndeterminate = true;
			} else {
				this.progress.IsIndeterminate = false;
				this.progress.Value = progress;
			}
		}
	}
}
